<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/motivasi', function() {
    return view('quote.motivasi', [
        'penulis' => 'Joni',
        'kalimat' => 'Jatuh adalah cara maju yang rasanya tidak enak. Bangkit, move on! Jangan menyerah!'
    ]);
});

Route::get('/inspirasi', function() {
    return view('quote.inspirasi');
});

Route::get('/new-quote', function() {
    return view('new-quote');
});

Route::post('/quote', function() {
    dd(Input::all());
});

Route::get('/edit-quote/{id}', function($id) {
    $quote = App\Quote::findOrFail($id);
    return view('edit-quote', compact('quote'));
});

Route::put('/quote/{id}', function($id) {
    $qoute = App\Quote::findOrFail($id);
    return view('edit-quote', compact('quote'));
});

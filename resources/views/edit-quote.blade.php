@extends('master')

@section('body')
{!! Form::model($quote, ['url'=>'/quote/' . $quote->id, 'method'=>'put']) !!}
  <legend>Update Quote</legend>

  @include('_quote-form')

  {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}

{!! Form::close() !!}
@stop

@extends('master')

@section('body')
{!! Form::open(['url'=>'/quote']) !!}
  <legend>New Quote</legend>

  @include('_quote-form')

  {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}

{!! Form::close() !!}
@stop

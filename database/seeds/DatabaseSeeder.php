<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);
        $quote = new \App\Quote;

        App\Quote::create([
            "author" => "Mario Teguh",
            "type" => "inspiration",
            "quote" => "Jadilah pria baik-baik."
        ]);

        App\Quote::create([
            "author" => "Andrie Wongso",
            "type" => "inspiration",
            "quote" => "1000 orang tidak percaya keamampuan kita, tidak masalah."
        ]);

        App\Quote::create([
            "author" => "Bob Sadino",
            "type" => "inspiration",
            "quote" => "Bergayalah sesuai isi dompetmu."
        ]);



        Model::reguard();
    }
}
